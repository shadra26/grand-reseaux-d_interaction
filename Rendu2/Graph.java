import java.util.List;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.io.FileReader;    
import java.io.BufferedReader; 
import java.io.IOException;    
import java.util.HashMap;     

 
class Graph { 

    int nb_art;         // Nombre d'arêtes du graphe
    Sommet[] sommets ;  // Le numéro de sommet vaut -1 si le sommet n'apparait pas dans le graphe (s'il n'a pas de voisins).
    int[] arete;        // tableau d'adjacence
    int nb_sommets;     // Nombre distincts de sommets

    Graph(String filename) {
	// construit à partir du fichier filename
        nb_art = nb_sommets = 0;

        // on implémente la renumérotation
        HashMap <Integer, Integer> sommetMap = new  HashMap <Integer, Integer>();
       
    
        
        // Passe 1 : lit le fichier, associe nom et numero de sommet, et calcule n et m
        try {
            BufferedReader read = new BufferedReader(new FileReader(filename));
            
            while(true) {
            int numLigne = 0; 
            // on lit le fichier ligne par ligne
            String line = read.readLine();
            if(line==null) // fin de fichier
                break;
            numLigne++;
            if(line.length()==0||line.charAt(0) == '#') // commentaire
                continue;
            int a = 0;
            for (int pos = 0; pos < line.length(); pos++)
                {
                // on converti char par char de ascii texte vers int
                char c = line.charAt(pos);
                if(c==' ' || c == '\t') // on a fini le premier sommet
                    {
                    if(sommetMap.get(a)==null) // on rencontre ce sommet pour la premiere fois
                        sommetMap.put(a, nb_sommets++); // le voila numéroté n
                    
                    a=0;
                    continue;
                    }
                if(c < '0' || c > '9')
                    {
                    System.out.println("ERREUR format ligne "+numLigne+"c = "+c+" valeur "+(int)c);
                    System.exit(1);
                    }
                a = 10*a + c - '0';
                }
            // et la on a fini le deuxieme
            if(sommetMap.get(a)==null) // on rencontre ce sommet pour la premiere fois
                sommetMap.put(a, nb_sommets++); // le voila numéroté n
            
            nb_art++; // on a fini un arc
            }
            read.close();
        }  catch (IOException e) {
            System.out.println("ERREUR entree/sortie sur "+filename);
            System.exit(1);
        }

        // Allouons maintenant nos deux tableaux
        sommets = new Sommet[nb_sommets];
        for(int i=0;i<nb_sommets;i++)
            sommets[i]=new Sommet();
            arete = new int[nb_art];
        
        
        // Passe 2 : relit le fichier et remplit adj et les noms de sommets. J'avoue un vilain copié/collé de la passe 1 :-(
        int posAdj=0 ; // position dans adj
        int x=0,y ; // on lit l'arete x--y
        int lastX=0; // num précédent sommet lu. Erreur si pas croissant.
        try {
            BufferedReader read = new BufferedReader(new FileReader(filename));

            while(true) {
            String line = read.readLine();
            if(line==null) // fin de fichier
                break;
            if(line.length()==0||line.charAt(0) == '#') // commentaire
                continue;
            int a = 0;
            for (int pos = 0; pos < line.length(); pos++)
                {
                boolean chiffres = true; // vaut vrai tant qu'on a lu que des chiffres. Sert à détecter le premier blanc.
                char c = line.charAt(pos);
                if(c==' ' || c == '\t')
                    {
                    if(chiffres) { // on lit le sommet a
                        chiffres = false; 
                        // on assume que a a été mis dans la HashMap a l'étape 1
                        x = (int) sommetMap.get(a); // son numero est x
                        if(x!=lastX) // on a changé de sommet ?
                        {								    
                            if(sommets[x].fini) // zut c'est la deuxieme fois qu'on voit x comme origine d'un arc
                            {
                                System.out.println("ERREUR graphe mal trié : liste d'adjacence du sommet "+ a +"non consecutive !");
                                System.exit(1);
                            }
                            else
                            sommets[x].fini = true;
                        }
                        lastX = x;
                        sommets[x].degre++;
                        sommets[x].numero = a;
                        if(sommets[x].debut==0 && x>0 ) // ce sommet n'a pas encore sont debutAdj
                        sommets[x].debut = posAdj;
                    }
                        
                    a=0;
                    continue;
                    }
                // on assume que c est entre '0' et '9' car vérifié lors de la apsse précédente
                a = 10*a + c - '0';
                }
            y = (int) sommetMap.get(a); // on vient de lire l'extremite de l'arete
            sommets[y].numero = a;
            arete[posAdj++] = y; // on l'insere dans la liste d'adj
            sommets[y].degre_ent += 1;

            }
            read.close();
        }  catch (IOException e) {
            System.out.println("ERREUR entree/sortie sur "+filename);
            System.exit(1);
        }
   
    }




//================================================ Cardialité entrante =====================================================



    //Calcul de la cardialité entrante d'un sommet v 
    public int cardialite(int v){

        for(int k = 0; k < nb_sommets; k++){ // Pour chaque k, on crée la liste des sommets de degré entrant < k O(k*n)

            List<Sommet> deg_inf_k = new ArrayList<Sommet>(); //Liste de sommets

            for(int i = 0; i < nb_sommets; i++){
                if (sommets[i].degre_ent < k)
                    deg_inf_k.add(sommets[i]);    
            }
        
            int j = 0;
            while (!deg_inf_k.isEmpty() && j < deg_inf_k.size()) {

                        
                if (deg_inf_k.get(j).present == false){
                    j++;
                    continue;

                } else {
                    deg_inf_k.get(j).present = false; // On enleve le sommet j s'il est present
            
                    int debut = deg_inf_k.get(j).debut;
                    int nb_deg_sortants = deg_inf_k.get(j).degre;

            
                    for(int d = debut; d < debut + nb_deg_sortants; d++){ // On décrémente le degré entrant de tous ses voisins sortants
                        if(sommets[arete[d]].degre_ent > 0 ){
                            sommets[arete[d]].degre_ent --;
                            if(sommets[arete[d]].degre_ent < k){
                                deg_inf_k.add(sommets[arete[d]]);
                            }
                        }           
                    }                      
                }      
            
                if ( deg_inf_k.get(j).numero == v ) { // Si on enlève v on s’arrête et on retourne k −1
                    return k-1;
                }
                j++;
            }
        }
        return -1;
    }


//================================================ Betweenness centrality =====================================================



    public void Npcc(int D) {

       //réinitialise tout les attributs npcc et visited d'un sommet 

        for (int i = 0; i < sommets.length; i++) {
            sommets[i].npcc = 0;
            sommets[i].visited = false;
        }

        // Utilise une file pour parcourir le graphe en largeur à partir du sommet D
        ArrayDeque<Integer> file = new ArrayDeque<Integer>(); 
        file.add(D); 
        sommets[D].npcc = 1;
        sommets[D].visited = true;
        sommets[D].dist = 0;
        
        while (!file.isEmpty()) {

            // Récupère le prochain sommet de la file et parcourt ses voisins
            int x = file.poll(); 
        
            for (int i = 0; i < sommets[x].degre; i++) { 
                
                int y = arete[sommets[x].debut + i];

                // Si le voisin n'a pas encore été visité, le marque comme visité et met à jour sa distance depuis le sommet D
                if (sommets[y].visited == false) {
                    sommets[y].visited = true; 
                    sommets[y].dist = 1 + sommets[x].dist;
                    file.add(y);
                }
                // Si la distance du voisin est égale à la distance de x + 1, alors ajoute le nombre de chemins les plus courts de x à y au nombre de chemins les plus courts de D à y
                if (sommets[y].dist == 1 + sommets[x].dist)
                    sommets[y].npcc += sommets[x].npcc;

            }
        }

    }



    public double bet(int v){

        double factor =1.0/((nb_sommets-1)*(nb_sommets-2));
        double sum = 0.0;


        // Calcule les attributs npcc et dist pour tous les sommets à partir du sommet v en utilisant l'algorithme Npcc
        Npcc(v);                    
        
        // Stocke les attributs npcc et dist de tous les sommets dans un tableau
        int[][] tab_v = new int[nb_sommets][2];

        for( int i=0; i<nb_sommets ; i++){
            tab_v[i][0] = sommets[i].dist; 
            tab_v[i][1] = sommets[i].npcc;
        }

        // Pour chaque paire de sommets s et t différents de v, calcule leur contribution à la centralité de v
        for(int s = 0; s < nb_sommets; s++){
            
            if (sommets[s].numero == sommets[v].numero){ 
                continue;
            }else{ 
                Npcc(s);
                double dist_s_v = sommets[v].dist;
                double npcc_s_v = sommets[v].npcc;
                for(int t = 0; t < nb_sommets; t++){

                    if (sommets[t].numero == sommets[v].numero || sommets[t].numero == sommets[s].numero) {
                        continue; 
                    }else{
                        double dist_s_t = sommets[t].dist;                                
                        double npcc_s_t = sommets[t].npcc;
                        double bet= 0.0;
                        double npcc_s_v_t = 0.0;
                        if (npcc_s_t == 0){
                            bet = 0.0;
                        } else {            
                                double dist_v_t = tab_v[t][0];
                                double npcc_v_t = tab_v[t][1];
                                if (dist_s_t == dist_s_v + dist_v_t ){
                                    npcc_s_v_t = npcc_s_v * npcc_v_t;
                                }else{
                                    npcc_s_v_t = 0.0;
                                }  
                            bet = npcc_s_v_t/npcc_s_t;
                        }
                        sum += bet;
                    }
                }
            }   
        }
        return factor * sum;

    }   


}


















