public class TP2 {    
    public static void main(String[] args) 
    {
	if (args.length != 3) {
	    System.out.println("ERREUR Usage : java TP2 commande nomFichier.txt numSommet");
	    return;
	} 


	Graph G = new Graph( args[1] );

	int D = Integer.parseInt(args[2]); // sommet de début du parcours (son nom, pas son numéro)

	int posD = -1; 
	for(int i=0; i<G.nb_sommets; i++)
	    if(G.sommets[i].numero ==D)
		{
		    posD = i; 
		    break;
		}

	if(posD == -1)
	    {
		return;
	    }

        String commande = args[0];

        // Check si la commande est de la forme attendue
        if (commande.length() != 2 || commande.charAt(0) != '-' || (commande.charAt(1) != 'c' && commande.charAt(1) != 'b')) {
            System.err.println("ERREUR commande : la commande doit être '-c' ou '-b'. ");
            System.exit(1);
        }

		// Affichage des sorties des algorithmes
		if (commande.charAt(1) == 'c'){
			System.out.println(G.cardialite(D));
		}else {
			System.out.println(G.bet(posD));
		}

	  
	}
	
	
}