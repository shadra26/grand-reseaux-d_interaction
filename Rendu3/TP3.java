import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

//Extension : // couleur sur les noeuds rebranchés 
             // couleur sur les arêtes créées après rebranchement

public class TP3 {
    public static void main(String[] args) {

        if (args.length != 7) {
            System.out.println(
                    "ERREUR input : java TP3 -w sortie n k p origine cible OU  java TP3 -k sortie c origine_x origine_y cible_x cible_y");
            return;
        }

        if (args[0].charAt(1) == 'w') {

            // Paramètres du modèle
            String fileName = args[1];
            int n = Integer.parseInt(args[2]); // Nombre de sommets
            int k = Integer.parseInt(args[3]);
            k = 2 * k; // Degré initial de chaque sommet
            double p = Double.parseDouble(args[4]); // Probabilité de rebranchement de chaque arête
            int origine = Integer.parseInt(args[5]);
            int destination = Integer.parseInt(args[6]);

            // Génération de l'anneau initial
            List<List<Integer>> adjList = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                List<Integer> neighbors = new ArrayList<>();
                for (int j = 1; j <= k / 2; j++) {
                    int neighbor1 = (i - j + n) % n;
                    int neighbor2 = (i + j) % n;
                    if (neighbor1 != i) {
                        neighbors.add(neighbor1);
                    }
                    if (neighbor2 != i) {
                        neighbors.add(neighbor2);
                    }
                }
                adjList.add(neighbors);
            }


            Random rand = new Random();
            for (int x = 0; x < n; x++) {
                for(int m =0; m< adjList.get(x).size(); m++){
                    if (rand.nextDouble() < p) {
                        List<Integer> eligibleNodes = new ArrayList<>();

                        for (int i = 0; i < n; i++) {
                            if (i != x && !adjList.get(x).contains(i)) { 
                                eligibleNodes.add(i);
                            }
                        }

                        eligibleNodes.add( adjList.get(x).get(m) );

                        if (!eligibleNodes.isEmpty()) {
                            int z = 0;
                            
                            if (eligibleNodes.size() > 1) {
                                z = rand.nextInt(eligibleNodes.size());                       
                            }

                            z = eligibleNodes.get(z);
                            if( z !=  adjList.get(x).get(m) ) {       //si on a pas tiré l'arête traité                           
                                int del = adjList.get(x).get(m);      //on va supprimer l'arête de base                         
                                adjList.get(x).remove(m);
                                adjList.get(del).remove( Integer.valueOf(x)); 
                                adjList.get(x).add(z);                  //on branche l'arête x--z                     
                                adjList.get(z).add(x);                  //on branche l'arête z--x
                                
                            }
                            
                        }
                    }


                }
            }

            // Gluttonous routing
            List<Integer> visitedNodes = new ArrayList<>();
            visitedNodes.add(origine);
            int current = origine;
            int distance = 0;
            boolean echec = false; 
            
            boolean[][] visitedEdges = new boolean[n][n];    // sert à marquer les arêtes visitées 


            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    visitedEdges[i][j] = false;
                }
            }
            
                while (current != destination) {

                    int max_curr_dest = 0;
                    int min_curr_dest = 0;

                    if (destination < current ){
                        max_curr_dest = current;
                        min_curr_dest = destination;

                    }
                    else{
                        max_curr_dest = destination;
                        min_curr_dest = current;
                    }

                    int dist_cd = Math.min(Math.abs(current - destination), Math.abs(min_curr_dest + n - max_curr_dest));
                    

                    List<Integer> neighbors = adjList.get(current);

                    int closestNeighbor = -1;
                    int closestDistance = Integer.MAX_VALUE;

                    for (int neighbor : neighbors) {

                        if (!visitedEdges[current][neighbor]) {
                        
                            int max_nei_dest = 0;
                            int min_nei_dest = 0;
                            
                            if (destination < neighbor ){
                                max_nei_dest = neighbor;
                                min_nei_dest = destination;
                                
                            }
                            else{
                                max_nei_dest = destination;
                                min_nei_dest = neighbor;
                            }

                            int dist = Math.min(Math.abs(neighbor - destination), Math.abs(min_nei_dest + n - max_nei_dest));
                            

                            if (dist < closestDistance) {   
                                closestNeighbor = neighbor;
                                closestDistance = dist;
                            }
                     }         
                    
                    }

                   if( (dist_cd < closestDistance) || (closestNeighbor == -1) ){ 
                        System.out.println("Glouton coincé, échec !");
                        echec = true;
                        break;
                    }

                    visitedNodes.add(closestNeighbor);

                    visitedEdges[current][closestNeighbor] = true;
                    visitedEdges[closestNeighbor][current] = true;

                    current = closestNeighbor;
                    distance++;
                
                }


            // Output

            if(!echec ){
                System.out.println(visitedNodes);
                System.out.println("longueur chemin " + distance);
            }    

            // Écriture du graphe dans un fichier
            try {
                FileWriter writerTxt = new FileWriter(fileName + ".txt");
                FileWriter writerDot = new FileWriter(fileName + ".dot");
                writerDot.write("graph "+ fileName +" {\n");
            
                writerDot.write("node[fixedsize=true,width=0.5];\n");
            
                double radius = 10.0; 
                Set<Integer> rebranchedNodes = new HashSet<>(); // create a set to store rebranched nodes
                for (int i = 0; i < n; i++) {
                    double angle = 2 * Math.PI * i / n; 
                    double x = radius * Math.cos(angle); // coordonnée x du noeud
                    double y = radius * Math.sin(angle); // coordonnée y du noeud
                    if (rebranchedNodes.contains(i)) {   // !!!!!!!!!  Change la couleur du noeud si il a été rebranché !!!!!!!
                        writerDot.write(i + " [pos=\"" + x + "," + y + "\", style=filled, fillcolor=cornflowerblue, fontcolor = \"darkblue\"];\n");
                    } else {
                        writerDot.write(i + " [pos=\"" + x + "," + y + "\"];\n");
                    }
            
                    List<Integer> neighbors = adjList.get(i);
                    Collections.sort(neighbors);
                    for (int neighbor : neighbors) {
                        writerTxt.write(i + " " + neighbor + "\n");
            
                        if (i < neighbor) {
                            if (neighbors.contains(neighbor) && rand.nextDouble() < p) {
                                
                                writerDot.write(i + " -- " + neighbor + " [color=\"dodgerblue3\"];\n"); // Couleur de l'arête ajoutée après rebranchement
                                rebranchedNodes.add(i); 
                                rebranchedNodes.add(neighbor);
                            } else {
                                writerDot.write(i + " -- " + neighbor + ";\n");
                            }
                        }
                    }
                }
            
                writerDot.write("}");
            
                writerTxt.close();
                writerDot.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (args[0].charAt(1) == 'k') {

            String fileName = args[1];
            int c = Integer.parseInt(args[2]); // side length of the square of the Kleinberg grid
            int origine_x = Integer.parseInt(args[3]);
            int origine_y = Integer.parseInt(args[4]);
            int cible_x = Integer.parseInt(args[5]);
            int cible_y = Integer.parseInt(args[6]);

            KleinbergGrid KB = new KleinbergGrid(c, origine_x, origine_y, cible_x, cible_y);

            List<Integer> visitedNodes = new ArrayList<>();

            visitedNodes = KB.routing_kleinberg();

            int distance = 0;
            for (Integer i : visitedNodes) {
                int node_i = i / c;
                int node_j = i % c;
    
                System.out.print(i + " ("+node_i +","+node_j+"); " );
                distance ++;
            }
            System.out.println("\nlongueur chemin :"+ (distance-1));


          

            try {
                // Write the vertexes and their corresponding neighbors to a file
                KB.writeToFile(fileName);
            } catch (IOException e) {
                System.out.println("An error occurred while writing the file: " + e.getMessage());
            }
        }

    }
}
