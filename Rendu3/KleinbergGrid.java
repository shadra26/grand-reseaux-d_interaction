import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// Extensions : colori a les noeuds et les arêtes du chemin suivi lors du routage

public class KleinbergGrid {

    private int gridSize;
    ArrayList<Integer>[][] grid ;

    int origine_x ;
    int origine_y;
    int cible_x;
    int cible_y;

    public KleinbergGrid(int c, int origine_x, int origine_y, int cible_x, int cible_y) {

        this.gridSize = c;
        this.grid = new ArrayList[c][c];
        this.cible_x = cible_x;
        this.cible_y = cible_y;
        this.origine_x = origine_x;
        this.origine_y = origine_y;

        for (int i = 0; i < c; i++) {
            for (int j = 0; j < c; j++) {
                grid[i][j] = new ArrayList<Integer>();
            }
        }

        for (int i = 0; i < c; i++) {
            for (int j = 0; j < c; j++) {
                int[] neighbors = getNeighbors(i, j);
            
                for (int k = 0; k < neighbors.length; k++) {
                    if (neighbors[k] >= 0 && neighbors[k] < c*c) {

                        grid[i][j].add(neighbors[k]);
                    }
                }
                int randomVertex = randomVertex(i, j);

                grid[i][j].add(randomVertex);
                int randomVertex_i = randomVertex / gridSize;
                int randomVertex_j = randomVertex % gridSize;

                int x = getIndex( i,  j);
                grid[randomVertex_i][randomVertex_j].add(x);


            }
        }
    }

    public int getIndex(int x, int y) {
        return x * gridSize + y;
    }


    public int[] getNeighbors(int x, int y) {
        List<Integer> neighborsList = new ArrayList<Integer>();

        if (x > 0) {
            neighborsList.add(getIndex(x-1, y));
        }
        if (y > 0) {
            neighborsList.add(getIndex(x, y-1));
        }
        if (x < gridSize-1) {
            neighborsList.add(getIndex(x+1, y));
        }
        if (y < gridSize-1) {
            neighborsList.add(getIndex(x, y+1));
        }
        
        int[] neighbors = new int[neighborsList.size()];
        for (int i = 0; i < neighbors.length; i++) {
            neighbors[i] = neighborsList.get(i);
        }
        
        return neighbors;
    }
    

    public double euclideanDistance(int x1, int y1, int x2, int y2) {
        
        double distance = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2)); 
        return distance;
    }

    
    public double sumInverseSquareDistances(int x, int y) {
        
        double sum = 0;

        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {

                if ((i != x || j != y)) {
                    boolean isNeighbor = false;
                    for (int k = 0; k < grid[x][y].size(); k++) {
                        
                        if (getIndex(i, j) ==  grid[x][y].get(k)) {
                            isNeighbor = true;
                            break;
                        }
                    }
                    if (!isNeighbor) {
                        double distance = euclideanDistance(x, y, i, j);
                        sum += 1.0 / (distance * distance);
                    }
                }
            }
        }
        return sum;
    }
    

    public double probabilityNeighbor(int xA, int yA, int xB, int yB,double S) {
        
        double distance = euclideanDistance(xA, yA, xB, yB);
        double probaY = 1.0 / (distance * distance * S);
        return probaY;
    }
    
    public int randomVertex(int xA, int yA) {
        double sumProb = 0;
        double S = sumInverseSquareDistances(xA, yA);
        double r = Math.random();


        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                if ((i != xA || j != yA)) {
                    boolean isNeighbor = false;
                    for (int k = 0; k < grid[xA][yA].size(); k++) {
                        
                        if (getIndex(i, j) ==  grid[xA][yA].get(k)) {
                            isNeighbor = true;
                            break;
                        }
                    }
                    if (!isNeighbor) {
                        double temp = sumProb;
                        double prob = probabilityNeighbor(xA, yA, i, j,S);
                        sumProb += prob;
                        if (temp < r && r <= sumProb ) {
                            return getIndex(i, j) ;
                        }

                    }
                }
            }  
        }


        return -1;
    }
    

// Gluttonous routing
public List<Integer> routing_kleinberg(){
    
    int origine = getIndex(origine_x,origine_y);
    int destination = getIndex(cible_x, cible_y);

    List<Integer> visitedNodes = new ArrayList<>();
    visitedNodes.add(origine);
    int current = origine;

    int destination_i = destination / gridSize;
    int destination_j = destination % gridSize;


    while (current != destination) {

        int current_i = current / gridSize;
        int current_j = current % gridSize;

        ArrayList<Integer> neighbors = grid[current_i][current_j];

        int closestNeighbor = -1;
        double closestDistance = Double.MAX_VALUE;
        double dist_cd = euclideanDistance(current_i,current_j,destination_i,destination_j);

        for (int neighbor : neighbors) {
            int neighbor_i = neighbor / gridSize;
            int neighbor_j = neighbor % gridSize;
            double dist = euclideanDistance( neighbor_i,  neighbor_j ,  destination_i,  destination_j);
            if (dist < closestDistance) {
                closestNeighbor = neighbor;
                closestDistance = dist;
            }
        }
        if(dist_cd < closestDistance){
            System.out.println("Glouton coincé, échec !");
            break;
        }

        visitedNodes.add(closestNeighbor);
        current = closestNeighbor;
    }

    return visitedNodes;
}


public void writeToFile(String filename) throws IOException {
    try {
        FileWriter writer = new FileWriter(filename+ ".txt");
        FileWriter writerDot = new FileWriter(filename + ".dot");

        writerDot.write("graph "+ filename +" {\n");

        
        List<Integer> visitedNodes = routing_kleinberg();

        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                int index = getIndex(i, j);
                if (visitedNodes.contains(index)) {
                    writerDot.write(index + " [ label=\"("+i +","+j+ ")\" style=filled fillcolor=green ]; \n"); // colori a les noeuds et les arêtes du chemin suivi lors du routage
                } else {
                    writerDot.write(index + " [ label=\"("+i +","+j+ ")\" ]; \n");
                }
            }
        }

        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                for (int k = 0; k < grid[i][j].size(); k++) {

                    int v = grid[i][j].get(k);
                    int neighborX = v/ gridSize;
                    int neighborY = v % gridSize;

                    int currentNode = getIndex(i, j);
                    int neighborNode = getIndex(neighborX, neighborY);

                    writer.write(currentNode+" "+ neighborNode +" \n");

                    if(i < neighborX || (i == neighborX && j < neighborY)){
                        if (visitedNodes.contains(currentNode) && visitedNodes.contains(neighborNode)) {
                            writerDot.write(currentNode + " -- " + neighborNode + " [color=green];\n"); // colori a les noeuds et les arêtes du chemin suivi lors du routage
                        } else {
                            writerDot.write(currentNode + " -- " + neighborNode + " ;\n");
                        }
                    }

                }
            }
        }

        writerDot.write("}");

        writer.close();
        writerDot.close();
    } catch (IOException e) {
        e.printStackTrace();
    }

}
    
}
