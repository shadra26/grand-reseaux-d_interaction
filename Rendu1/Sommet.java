public class Sommet{

    int numero; 
    int degre;  // le nombre de voisins du sommet 
    int debut; // Nous permet de garder un pointeur sur la première apparition du sommet dans la liste d'adjacence 

    public Sommet(int num , int deg){
        this.debut=-1;
        this.numero = num;
        this.degre = deg;
    }

}