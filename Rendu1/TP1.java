import java.io.FileReader;     
import java.io.BufferedReader; 
import java.io.IOException; 
import java.util.Set;
import java.util.HashSet;

class TP1
{ 
    public static void main(String args[]){

        if (args.length != 2) {
            System.out.println("ERREUR argument commande non valide, faire : java TP1 Fichier sommet");
            return;
        }

        String line1 = "";
        int num_sommet_max = 0;
        int nb_arete = 0;
        int num_sommet = 0;


        try {  //Première lecture pour récuperer le numero du sommet max, le nombre distincts de sommets et le nombre d'arêtes

            BufferedReader file = new BufferedReader(new FileReader(args[0]));        
            Set<Integer> sommets = new HashSet<Integer>();

            while (file.ready()) {         

                line1 = file.readLine();
                if(line1.length()==0||line1.charAt(0) == '#')
                     continue;
                     nb_arete++;
                String[] vertexString = line1.split("[\t  ]+");
                sommets.add(Integer.parseInt(vertexString[0]));
                sommets.add(Integer.parseInt(vertexString[1]));     
                num_sommet_max = Math.max(num_sommet_max, Math.max( Integer.parseInt(vertexString[0]), Integer.parseInt(vertexString[1])));                
            }
            num_sommet = sommets.size();
        
            file.close();
        } catch (IOException e) {
            System.out.println("ERREUR entrée fichier : "+ args[0]);
            e.printStackTrace();
        }


        Graph graph = new Graph( num_sommet_max + 1, nb_arete, num_sommet);
        for( int i=0; i< graph.num_sommet_max; i++)
            graph.sommets[i] = new Sommet(-1, 0) ; // Le numéro de sommet vaut -1 si le sommet n'apparait pas dans le graphe (s'il n'a pas de voisins). 
    
        
        try { // Deuxième lecture pour remplire le tableau du sommets, mettre à jour les degrès et les arêtes

            String line2 = "";
            int origine = 0;
            int dest = 0;
            String string_origine = "";
            String string_dest = "";
            int num_line = 0;

            BufferedReader file2 = new BufferedReader(new FileReader(args[0]));

            while (file2.ready()) {

                line2 = file2.readLine();
                if(line2.length()== 0||line2.charAt(0) == '#') 
                    continue;
                    
                String[] orig_dest_String = line2.split("[\t  ]+");

                string_origine = orig_dest_String[0];
                string_dest =  orig_dest_String[1];

                origine = Integer.parseInt(string_origine);
                dest = Integer.parseInt(string_dest);
                

                if( graph.sommets[origine].debut == -1 ) graph.sommets[origine].debut = num_line; // Si c'est la première apparition du sommet (qui est une origine),
                                                                                                  // garder l'indice de son premier voisin dans le tableau d'adjacence.           
                                                                                               
                graph.sommets[dest].numero = dest;
                graph.sommets[origine].numero = origine;        
                graph.arete[num_line] = dest;      
                num_line++;
                graph.sommets[origine].degre++;

            }

            file2.close();
            int x = Integer.parseInt(args[1]);

            if (x > graph.num_sommet_max | x < 0) {
                System.out.println("ERREUR entrée sommet : le sommet "+ x +" n'appartient pas au graphe.");
                return;
            }
             
            // Affichage des sorties des algorithmes
            System.out.println(graph.taille_Sn()+"\n"+graph.nb_arete()+"\n"+graph.max_degre()+"\n"+graph.nb_acc_sommet(x)+"\n"+graph.longestShortestPath(x)+"\n"+graph.nb_conn_components(x)+"\n");     
   

        } catch (IOException e) {
            System.out.println("ERREUR entrée fichier : "+ args[0]);
            e.printStackTrace();
        }
    }  
}