import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;


public class Graph {

    int num_sommet_max; // Numèro du plus grand sommet du graphe
    int nb_art;         // Nombre d'arêtes du graphe
    Sommet[] sommets ;  // Le numéro de sommet vaut -1 si le sommet n'apparait pas dans le graphe (s'il n'a pas de voisins).
    int[] arete;        
    int nb_sommets;     // Nombre distincts de sommets


    public Graph(int num_sommet_max, int nb_arr, int nb_sommets){
        this.nb_sommets = nb_sommets  ;
        this.num_sommet_max = num_sommet_max;
        this.nb_art = nb_arr;
        this.sommets = new Sommet[num_sommet_max]; 
        this.arete = new int [nb_arr];
    }

    //Renvoie la taille Sn du tableau de sommets 
    public int taille_Sn(){
        return this.nb_sommets;
    }
    
    //Retourne le nombre exact m d’arêtes
    public int nb_arete(){
        return this.nb_art;
    }

    //Retourne le degré maximum d’un sommet (nombre de voisins du sommets qui en a le plus)
    public int max_degre(){
        int max_deg = sommets[0].degre ;
        for(int i=1 ; i<num_sommet_max ; i++){
            if (max_deg < sommets[i].degre){
                max_deg = sommets[i].degre;
            }
        }
        return max_deg;
    }

    //Rznvoie le nombre de sommets accessibles depuis x 
    public int nb_acc_sommet(int x) {
        
        boolean[] visited = new boolean[num_sommet_max];    // Crée un tableau de booléens pour marquer les sommets visités
        Queue<Integer> queue = new LinkedList<>();  // Initialise une file pour stocker les sommets à visiter

        int count = 0;
        queue.add(x); 
        count++;
        visited[x] = true;

        while (!queue.isEmpty()) {
            int current = queue.poll(); 
            
            for (int i = 0; i < sommets[current].degre; i++) {          
                int v = arete[sommets[current].debut + i] ;        // Récupère le sommet adjacent

                if (!visited[v]) { 
                    visited[v] = true; 
                    queue.add(v);
                    count++; 
                }
            }
        }
        return count;
    }

    // Renvoie l’eccentricité de x, 
    public int longestShortestPath(int x) {

        int[] dist = new int[num_sommet_max];    //On crée un tableau pour stocker la distance entre x et chaque sommet
        Arrays.fill(dist, -1);       // On initialise toutes les distances à -1 pour indiquer qu'elles ne sont pas encore déterminées
        Queue<Integer> queue = new LinkedList<>();

        int max = 0;
        dist[x] = 0;    // La distance entre x et x est 0 
        queue.add(x);

        while (!queue.isEmpty()) {
            int current = queue.poll();     // On prend le prochain sommet à traiter

            for (int i = 0; i < sommets[current].degre; i++) { 
                int v = arete[sommets[current].debut + i] ;
                if (dist[v] == -1) {
                    dist[v] = dist[current] + 1;    // On met à jour la distance en prenant en compte la distance entre x et le sommet courant
                    queue.add(v);
                }
            }
        }
        
        for (int i = 0; i < num_sommet_max; i++) {
            max = Math.max(max, dist[i]);   // On trouve la distance maximale
        }
        return max;
    }


    /* Retourne le nombre de composantes connexes si le graphe est orienté symétrique
    sinon affiche n’importe quel nombre, */
    public int nb_conn_components(int x) {
        
        int count = 0;
        boolean[] visited = new boolean[num_sommet_max];
        Queue<Integer> queue = new LinkedList<>();

        // Parcours de tous les sommets
        for (int i = 0; i < num_sommet_max; i++) {

            if (!visited[i]) {      // Si le sommet n'a pas été visité
                if(sommets[i].numero != -1) count++; 
                queue.add(i);       // ajouter le sommet à la file
                visited[i] = true;          // marquer le sommet comme visité

                while (!queue.isEmpty()) {
                    int current = queue.poll();
                    
                    for (int j = 0; j < sommets[current].degre; j++) {      // pour chaque arête reliée au sommet courant
                        int v = arete[sommets[current].debut + j] ;        // récupère le sommet voisin
                        if (!visited[v]) {
                            visited[v] = true;
                            queue.add(v);
                        }
                    }
                }
            }
        }
        return count;
    }
}

